﻿using UnityEngine;
using System.Collections;

public class Cursor_Control : MonoBehaviour {

	public GameObject floorController;
	public GameObject myPlayer;
	public int playerNumber;
	public Material selectedMat;
	public Material deselectedMat;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		float speed = 80;
		float dist = speed * Vector3.Distance(floorController.GetComponent<Floor_Control>().selectedPlayer.transform.position, transform.position) + 12;
		if(myPlayer == floorController.GetComponent<Floor_Control>().selectedPlayer && myPlayer.GetComponent<Player_Unit>().canMove == false)
		{
			GetComponent<Renderer>().material = selectedMat;
			if (myPlayer == floorController.GetComponent<Floor_Control>().Player[0])
			{
				Vector3 selectionTrack = transform.position;
				selectionTrack.z = floorController.GetComponent<Floor_Control>().p1SavedCoord[0];
				selectionTrack.x = floorController.GetComponent<Floor_Control>().p1SavedCoord[1];
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
			if (myPlayer == floorController.GetComponent<Floor_Control>().Player[1])
			{
				Vector3 selectionTrack = transform.position;
				selectionTrack.z = floorController.GetComponent<Floor_Control>().p2SavedCoord[0];
				selectionTrack.x = floorController.GetComponent<Floor_Control>().p2SavedCoord[1];
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
			if (myPlayer == floorController.GetComponent<Floor_Control>().Player[2])
			{
				Vector3 selectionTrack = transform.position;
				selectionTrack.z = floorController.GetComponent<Floor_Control>().p3SavedCoord[0];
				selectionTrack.x = floorController.GetComponent<Floor_Control>().p3SavedCoord[1];
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
			if (myPlayer == floorController.GetComponent<Floor_Control>().Player[3])
			{
				Vector3 selectionTrack = transform.position;
				selectionTrack.z = floorController.GetComponent<Floor_Control>().p4SavedCoord[0];
				selectionTrack.x = floorController.GetComponent<Floor_Control>().p4SavedCoord[1];
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
		}
		else
		{
			GetComponent<Renderer>().material = deselectedMat;
			if (myPlayer == floorController.GetComponent<Floor_Control>().Player[0])
			{
				Vector3 selectionTrack = transform.position;
				selectionTrack.z = floorController.GetComponent<Floor_Control>().p1SavedCoord[0];
				selectionTrack.x = floorController.GetComponent<Floor_Control>().p1SavedCoord[1];
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
			if (myPlayer == floorController.GetComponent<Floor_Control>().Player[1])
			{
				Vector3 selectionTrack = transform.position;
				selectionTrack.z = floorController.GetComponent<Floor_Control>().p2SavedCoord[0];
				selectionTrack.x = floorController.GetComponent<Floor_Control>().p2SavedCoord[1];
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
			if (myPlayer == floorController.GetComponent<Floor_Control>().Player[2])
			{
				Vector3 selectionTrack = transform.position;
				selectionTrack.z = floorController.GetComponent<Floor_Control>().p3SavedCoord[0];
				selectionTrack.x = floorController.GetComponent<Floor_Control>().p3SavedCoord[1];
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
			if (myPlayer == floorController.GetComponent<Floor_Control>().Player[3])
			{
				Vector3 selectionTrack = transform.position;
				selectionTrack.z = floorController.GetComponent<Floor_Control>().p4SavedCoord[0];
				selectionTrack.x = floorController.GetComponent<Floor_Control>().p4SavedCoord[1];
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
		}
	}
}
