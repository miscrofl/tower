﻿using UnityEngine;
using System.Collections;

public class Camera_Control : MonoBehaviour {

	private GameObject lookAtTarget;
	private Vector3 velocity;
	
	public GameObject selectedPlayer;
	public Vector3 cameraTarget;
	public GameObject floorController;
	public float cameraHeight;
	public float maxCameraHeight = 25f;
	public float minCameraHeight = 1.5f;
	public float cameraStep = 0.5f;
	public float cameraX;
	public float cameraZ;
	public float focusTime = 60f;
	public int cameraMode = 1;
	public bool timeOut = false;
	public float timer;
	// Use this for initialization
	void Start () 
	{
		cameraTarget = selectedPlayer.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		selectedPlayer = floorController.GetComponent<Floor_Control>().selectedPlayer;
		Timer();
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			if(cameraTarget != selectedPlayer.transform.position)
				cameraTarget = Vector3.MoveTowards(cameraTarget, selectedPlayer.transform.position, 50 * Time.deltaTime);
			if (cameraMode == 1  && timeOut == false)
			{
				cameraMode = 4;
				timeOut = true;
				timer = 0.3f;
			}
			if (cameraMode == 2  && timeOut == false)
			{
				cameraMode = 1;
				timeOut = true;
				timer = 0.3f;
			}
			if (cameraMode == 3  && timeOut == false)
			{
				cameraMode = 2;
				timeOut = true;
				timer = 0.3f;
			}
			if (cameraMode == 4  && timeOut == false)
			{
				cameraMode = 3;
				timeOut = true;
				timer = 0.3f;
			}
		}
		if (Input.GetKey(KeyCode.RightArrow) && timeOut == false)
		{
			if(cameraTarget != selectedPlayer.transform.position)
				cameraTarget = Vector3.MoveTowards(cameraTarget, selectedPlayer.transform.position, 50 * Time.deltaTime);
			if (cameraMode == 1  && timeOut == false)
			{
				cameraMode = 2;
				timeOut = true;
				timer = 0.3f;
			}
			if (cameraMode == 2  && timeOut == false)
			{
				cameraMode = 3;
				timeOut = true;
				timer = 0.3f;
			}
			if (cameraMode == 3  && timeOut == false)
			{
				cameraMode = 4;
				timeOut = true;
				timer = 0.3f;
			}
			if (cameraMode == 4  && timeOut == false)
			{
				cameraMode = 1;
				timeOut = true;
				timer = 0.3f;
			}
		}
		//
		if (cameraMode == 1)
		{
			cameraX = 4;
			cameraZ = 4;
		}
		if (cameraMode == 2)
		{
			cameraX = -4;
			cameraZ = 4;
		}
		if (cameraMode == 3)
		{
			cameraX = -4;
			cameraZ = -4;
		}
		if (cameraMode == 4)
		{
			cameraX = 4;
			cameraZ = -4;
		}
		//
		if (cameraHeight > maxCameraHeight)
		cameraHeight = maxCameraHeight;
		if (cameraHeight < minCameraHeight)
		cameraHeight = minCameraHeight;
		if  (Input.GetKey(KeyCode.DownArrow) && cameraHeight < maxCameraHeight)
		{
			if(cameraTarget != selectedPlayer.transform.position)
				cameraTarget = Vector3.MoveTowards(cameraTarget, selectedPlayer.transform.position, 50 * Time.deltaTime);
			cameraHeight += cameraStep;
		}
		if  (Input.GetKey(KeyCode.UpArrow) && cameraHeight > minCameraHeight)
		{
			if(cameraTarget != selectedPlayer.transform.position)
				cameraTarget = Vector3.MoveTowards(cameraTarget, selectedPlayer.transform.position, 50 * Time.deltaTime);
			cameraHeight -= cameraStep;
		}
		//

		if(cameraTarget != selectedPlayer.transform.position)
		{
			cameraTarget = Vector3.SmoothDamp(cameraTarget, selectedPlayer.transform.position, ref velocity, focusTime * 2 * Time.deltaTime);
		}
		
		Vector3 playerTrack = selectedPlayer.transform.position;
		playerTrack.y = selectedPlayer.transform.position.y + cameraHeight;
		playerTrack.x = selectedPlayer.transform.position.x + cameraX;
		playerTrack.z = selectedPlayer.transform.position.z + cameraZ;
		transform.position = Vector3.SmoothDamp(transform.position, playerTrack, ref velocity, focusTime * Time.deltaTime);
		transform.LookAt(cameraTarget);
	}
	
	
	private void Timer()
	{
		if (timeOut && timer > 0)
		{
			timer -= Time.deltaTime;
		}
		if (timer < 0)
		{
			timer = 0;
			timeOut = false;
		}
	}
}
