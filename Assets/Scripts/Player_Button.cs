﻿using UnityEngine;
using System.Collections;

public class Player_Button : MonoBehaviour {

	public GameObject myPlayer;
	public GameObject floorController;
	public GameObject moveIndicator;
	
	void Start ()
	{
	//	for (int i = 1; i < myPlayer.GetComponent<Player_Unit>().moveRange+1; i++)
	//		{
	//			GameObject Move_Indicator;
	//			Move_Indicator = Instantiate(moveIndicator, new Vector3(transform.position.x + Screen.width/20 + (i * (Screen.width/24)), transform.position.y, transform.position.z), Quaternion.identity) as GameObject;
	//			Move_Indicator.GetComponent<Move_Indicator_Control>().myPlayer = myPlayer;
	//			Move_Indicator.GetComponent<Move_Indicator_Control>().moveNumber = i;
	//			myPlayer.GetComponent<Player_Unit>().movePhase[i] = Move_Indicator;
	//			Move_Indicator.transform.parent = transform;
	//		}		
	}
	public void PlayerClick ()
	{
		//Debug.Log("Player " + myPlayer.GetComponent<Player_Unit>().playerNumber + " on space " + myPlayer.GetComponent<Player_Unit>().myCoord[0] + ", " + myPlayer.GetComponent<Player_Unit>().myCoord[1]);
		if(myPlayer.GetComponent<Player_Unit>().playerNumber == 1)
		{
		floorController.GetComponent<Floor_Control>().Player[0].GetComponent<Player_Unit>().selected = true;
		floorController.GetComponent<Floor_Control>().Player[1].GetComponent<Player_Unit>().selected = false;
		floorController.GetComponent<Floor_Control>().Player[2].GetComponent<Player_Unit>().selected = false;
		floorController.GetComponent<Floor_Control>().Player[3].GetComponent<Player_Unit>().selected = false;
		floorController.GetComponent<Floor_Control>().selectedPlayer = myPlayer;
		myPlayer.GetComponent<Player_Unit>().Invoke("GetTile", 0.1f);
		myPlayer.GetComponent<Player_Unit>().resetSelection = false;
		}
		if(myPlayer.GetComponent<Player_Unit>().playerNumber == 2)
		{
		floorController.GetComponent<Floor_Control>().Player[0].GetComponent<Player_Unit>().selected = false;
		floorController.GetComponent<Floor_Control>().Player[1].GetComponent<Player_Unit>().selected = true;
		floorController.GetComponent<Floor_Control>().Player[2].GetComponent<Player_Unit>().selected = false;
		floorController.GetComponent<Floor_Control>().Player[3].GetComponent<Player_Unit>().selected = false;
		floorController.GetComponent<Floor_Control>().selectedPlayer = myPlayer;
		myPlayer.GetComponent<Player_Unit>().Invoke("GetTile", 0.1f);
		myPlayer.GetComponent<Player_Unit>().resetSelection = false;
		}
		if(myPlayer.GetComponent<Player_Unit>().playerNumber == 3)
		{
		floorController.GetComponent<Floor_Control>().Player[0].GetComponent<Player_Unit>().selected = false;
		floorController.GetComponent<Floor_Control>().Player[1].GetComponent<Player_Unit>().selected = false;
		floorController.GetComponent<Floor_Control>().Player[2].GetComponent<Player_Unit>().selected = true;
		floorController.GetComponent<Floor_Control>().Player[3].GetComponent<Player_Unit>().selected = false;
		floorController.GetComponent<Floor_Control>().selectedPlayer = myPlayer;
		myPlayer.GetComponent<Player_Unit>().Invoke("GetTile", 0.1f);
		myPlayer.GetComponent<Player_Unit>().resetSelection = false;
		}
		if(myPlayer.GetComponent<Player_Unit>().playerNumber == 4)
		{
		floorController.GetComponent<Floor_Control>().Player[0].GetComponent<Player_Unit>().selected = false;
		floorController.GetComponent<Floor_Control>().Player[1].GetComponent<Player_Unit>().selected = false;
		floorController.GetComponent<Floor_Control>().Player[2].GetComponent<Player_Unit>().selected = false;
		floorController.GetComponent<Floor_Control>().Player[3].GetComponent<Player_Unit>().selected = true;
		floorController.GetComponent<Floor_Control>().selectedPlayer = myPlayer;
		myPlayer.GetComponent<Player_Unit>().Invoke("GetTile", 0.1f);
		myPlayer.GetComponent<Player_Unit>().resetSelection = false;
		}
	}
}
