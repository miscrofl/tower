﻿using UnityEngine;
using System.Collections;

public class Floor_Unit : MonoBehaviour {

	public int[] floorCoord = new int[2]; //Floor coordinates
	public GameObject floorController; //Floor controller host object which stores level data
	public bool withinMoveRange; //Bool return to check against selected player's range
	public GameObject selectedPlayer;	//Selected player
	public GameObject prevPlayer; //Previously selected player
	public int tileDistance; //Distance from selected player
	public GameObject occupant = null;
	
	public bool highlighted = false; //Mouse hover highlight
	public int[] highlightedPath = new int[3]; //Player pathing highlight per player (index 0 = player1, etc, int value 0 = off 1 = on)
	public bool isSelected = false; //Selected highlight
	
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		selectedPlayer = floorController.GetComponent<Floor_Control>().selectedPlayer;
		if(floorController.GetComponent<Floor_Control>().globalTurn == 0)
		{
			prevPlayer = selectedPlayer;
		}
		
		//withinMoveRange check for Range Markers, Range Marker layer 9 = movement, 10 = attack
		RaycastHit inRange;
		int layerMask = 1 << 9;
		if (Physics.Raycast(transform.position, Vector3.up, out inRange, 0.2f, layerMask))
		{
			inRange.collider.GetComponent<Floor_Unit>().withinMoveRange = true;
		}
		else
		{
			withinMoveRange = false;
		}
	}
	void OnMouseDown()
	{
		//start pathfind function of player to get all blocks between
		if(floorController.GetComponent<Floor_Control>().globalTurn == 0 && withinMoveRange == true && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
		{
			floorController.GetComponent<Floor_Control>().selectedPlayer.GetComponent<Player_Unit>().generate = true; //Can generate range markers
			floorController.GetComponent<Floor_Control>().selectedPlayer.GetComponent<Player_Unit>().Invoke("HighlightPath", 0.15f); //Raytraced path find function
			floorController.GetComponent<Floor_Control>().selectedPlayer.GetComponent<Player_Unit>().resetSelection = true; //Because clicking on a new block should replace the saved block, but clicking on a player or his icon should not
			if(floorController.GetComponent<Floor_Control>().selectedPlayer == floorController.GetComponent<Floor_Control>().Player[0])
			{
				floorController.GetComponent<Floor_Control>().p1SavedCoord = floorCoord;
			}
			if(floorController.GetComponent<Floor_Control>().selectedPlayer == floorController.GetComponent<Floor_Control>().Player[1])
			{
				floorController.GetComponent<Floor_Control>().p2SavedCoord = floorCoord;
			}
			if(floorController.GetComponent<Floor_Control>().selectedPlayer == floorController.GetComponent<Floor_Control>().Player[2])
			{
				floorController.GetComponent<Floor_Control>().p3SavedCoord = floorCoord;
			}
			if(floorController.GetComponent<Floor_Control>().selectedPlayer == floorController.GetComponent<Floor_Control>().Player[3])
			{
				floorController.GetComponent<Floor_Control>().p4SavedCoord = floorCoord;
			}
		}
	}
	void OnMouseEnter()
	{
		//higgghlliiiiiiiiiiiiiiiiighhhttttsss
		if (floorController.GetComponent<Floor_Control>().globalTurn == 0 && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
		{
			highlighted = true;
			floorController.GetComponent<Floor_Control>().highlightedFloor = floorCoord;
		}
		else
		{
			highlighted = false;
		}
	}
	void OnMouseExit()
	{
		highlighted = false;
	}
}
