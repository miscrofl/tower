﻿using UnityEngine;
using System.Collections;

public class Range_Marker : MonoBehaviour {

	public GameObject originPlayer;
	public int playerNumber;
	public int number;
	public Material rangeIndicator;
	public string type;
	public Texture[] counter = new Texture[16];
	public GameObject camera;
	// Use this for initialization
	void Start () 
	{
		if(type == "Move")
		{
			GetComponent<Renderer>().material = rangeIndicator;
			RaycastHit inRange;
			int layerMask = 1 << 8;
			if (Physics.Raycast(transform.position, Vector3.down, out inRange, 1.0f, layerMask))
			{
				inRange.collider.GetComponent<Floor_Unit>().withinMoveRange = true;
			}
			else
			{
				Destroy(gameObject);
			}
		}
		if(type == "Attack")
		{
			GetComponent<Renderer>().material = rangeIndicator;
			RaycastHit inRange;
			int layerMask = 1 << 8;
			if (Physics.Raycast(transform.position, Vector3.down, out inRange, 1.0f, layerMask))
			{
				//inRange.collider.GetComponent<Floor_Unit>().withinAttackRange = true;
			}
			else
			{
				//Destroy(gameObject);
			}
		}
		if(type == "MoveQueue")
		{
			GetComponent<Renderer>().material = rangeIndicator;
			RaycastHit inRange;
			int layerMask = 1 << 8;
			if (Physics.Raycast(transform.position, Vector3.down, out inRange, 1.0f, layerMask))
			{
			}
			else
			{
				Destroy(gameObject);
			}
		}
		if(type == "AttackQueue")
		{
			GetComponent<Renderer>().material = rangeIndicator;
			RaycastHit inRange;
			int layerMask = 1 << 8;
			if (Physics.Raycast(transform.position, Vector3.down, out inRange, 1.0f, layerMask))
			{
			}
			else
			{
				Destroy(gameObject);
			}
		}
		camera = originPlayer.GetComponent<Player_Unit>().camera;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(type == "Move")
		{
			RaycastHit inRange;
			int layerMask = 1 << 8;
			if (Physics.Raycast(transform.position, Vector3.down, out inRange, 1.0f, layerMask))
			{
				inRange.collider.GetComponent<Floor_Unit>().withinMoveRange = true;
			}
			else
			{
				Destroy(gameObject);
			}
		}
		if(type == "Attack")
		{
			RaycastHit inRange;
			int layerMask = 1 << 8;
			if (Physics.Raycast(transform.position, Vector3.down, out inRange, 1.0f, layerMask))
			{
				//inRange.collider.GetComponent<Floor_Unit>().withinAttackRange = true;
			}
			else
			{
				Destroy(gameObject);
			}
		}
		if(type == "MoveQueue" || type == "AttackQueue")
		{
			if(Input.GetKeyDown(KeyCode.LeftAlt))
			{
				Invoke("ToggleVisible", 0.01f);
			}
			if(originPlayer.GetComponent<Player_Unit>().IsInvoking("HighlightPath"))
			{
				Destroy(gameObject);
			}
		}
		if(originPlayer.GetComponent<Player_Unit>().selected == false && (type == "Move" || type == "Attack"))
		{
			Destroy(gameObject);
		}
		if(originPlayer.GetComponent<Player_Unit>().selected == true && originPlayer.GetComponent<Player_Unit>().IsInvoking("GetTile") && (type == "Move" || type == "Attack"))
		{
			Destroy(gameObject);
		}
		
		
		if(number > 0)
		{
			var rend = GetComponent<Renderer>();
			rend.material.SetTexture("_MainTex", counter[number]);
		}
		
	}
	
	void ToggleVisible ()
	{
		
		if(GetComponent<Renderer>().enabled == true)
		{
			GetComponent<Renderer>().enabled = false;
		}
		else
		{
			GetComponent<Renderer>().enabled = true;
		}
		CancelInvoke("ToggleVisible");
	}
}
