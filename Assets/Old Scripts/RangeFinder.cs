﻿using UnityEngine;
using System.Collections;

public class RangeFinder : MonoBehaviour {
	
	public bool generateMove = true;
	public bool generateAttack = true;
	public GameObject FloorController;
	public GameObject Player1;
	public GameObject Player2;
	public GameObject Player3;
	public GameObject Player4;
	public GameObject activePlayer;
	public GameObject RangeMarker;
	public Material moveRangeIndicator;
	public Material attackRangeIndicator;
	public int moveRange;
	public int attackRange;
	public string activePlayerString;
	// Use this for initialization
	void Start () 
	{
		activePlayer = FloorController.GetComponent<FloorControl>().activePlayer;
	}
	
	// Update is called once per frame
	void Update () 
	{
		moveRange = activePlayer.GetComponent<PlayerUnit>().moveRange;
		attackRange = activePlayer.GetComponent<PlayerUnit>().attackRange;
		if (activePlayer == Player1)
		{
			activePlayerString = "Player 1";
			transform.position = activePlayer.transform.position;
		}
		if (activePlayer == Player2)
		{
			activePlayerString = "Player 2";
			transform.position = activePlayer.transform.position;
		}
		if (activePlayer == Player3)
		{
			activePlayerString = "Player 3";
			transform.position = activePlayer.transform.position;
		}
		if (activePlayer == Player4)
		{
			activePlayerString = "Player 4";
			transform.position = activePlayer.transform.position;
		}
		if (generateMove == true)
		{
			gameObject.layer = 9;
			int maxRange = moveRange;
			for (int nRange = 0; nRange - moveRange < moveRange; nRange++)//SECornerFill
			{
				for (int startRange = 0; startRange + nRange < moveRange; startRange++)//South-East
				{
					GameObject RangeMarkSE;
					RangeMarkSE = Instantiate(RangeMarker, new Vector3(transform.position.x + nRange + 1, transform.position.y-0.4f, transform.position.z + startRange), Quaternion.identity) as GameObject;
					RangeMarkSE.GetComponent<RangeChecker>().originBlock = this.gameObject;
					RangeMarkSE.GetComponent<RangeChecker>().playerString = activePlayerString;
					RangeMarkSE.GetComponent<RangeChecker>().rangeIndicator = moveRangeIndicator;
					RangeMarkSE.GetComponent<RangeChecker>().type = "Move";
					RangeMarkSE.transform.parent = transform;
					generateMove = false;
				}
				for (int startRange = 0; startRange + nRange < moveRange; startRange++)//South-West
				{
					GameObject RangeMarkSW;
					RangeMarkSW = Instantiate(RangeMarker, new Vector3(transform.position.x + nRange, transform.position.y-0.4f, transform.position.z - startRange - 1), Quaternion.identity) as GameObject;
					RangeMarkSW.GetComponent<RangeChecker>().originBlock = this.gameObject;
					RangeMarkSW.GetComponent<RangeChecker>().playerString = activePlayerString;
					RangeMarkSW.GetComponent<RangeChecker>().rangeIndicator = moveRangeIndicator;
					RangeMarkSW.GetComponent<RangeChecker>().type = "Move";
					RangeMarkSW.transform.parent = transform;
					generateMove = false;
				}
				for (int startRange = 0; startRange + nRange < moveRange; startRange++)//North-West
				{
					GameObject RangeMarkNW;
					RangeMarkNW = Instantiate(RangeMarker, new Vector3(transform.position.x - startRange - 1, transform.position.y-0.4f, transform.position.z - nRange), Quaternion.identity) as GameObject;
					RangeMarkNW.GetComponent<RangeChecker>().originBlock = this.gameObject;
					RangeMarkNW.GetComponent<RangeChecker>().playerString = activePlayerString;
					RangeMarkNW.GetComponent<RangeChecker>().rangeIndicator = moveRangeIndicator;
					RangeMarkNW.GetComponent<RangeChecker>().type = "Move";
					RangeMarkNW.transform.parent = transform;
					generateMove = false;
				}
				for (int startRange = 0; startRange + nRange < moveRange; startRange++)//North-East
				{
					GameObject RangeMarkNE;
					RangeMarkNE = Instantiate(RangeMarker, new Vector3(transform.position.x - startRange, transform.position.y-0.4f, transform.position.z + nRange + 1), Quaternion.identity) as GameObject;
					RangeMarkNE.GetComponent<RangeChecker>().originBlock = this.gameObject;
					RangeMarkNE.GetComponent<RangeChecker>().playerString = activePlayerString;
					RangeMarkNE.GetComponent<RangeChecker>().rangeIndicator = moveRangeIndicator;
					RangeMarkNE.GetComponent<RangeChecker>().type = "Move";
					RangeMarkNE.transform.parent = transform;
					generateMove = false;
				}
			}
		}
		if (generateAttack == true)
		{
			gameObject.layer = 10;
			int maxRange = attackRange;
			for (int nRange = 0; nRange - attackRange < attackRange; nRange++)//SECornerFill
			{
				for (int startRange = 0; startRange + nRange < attackRange; startRange++)//South-East
				{
					GameObject RangeMarkSE;
					RangeMarkSE = Instantiate(RangeMarker, new Vector3(transform.position.x + nRange + 1, transform.position.y-0.39f, transform.position.z + startRange), Quaternion.identity) as GameObject;
					RangeMarkSE.GetComponent<RangeChecker>().originBlock = this.gameObject;
					RangeMarkSE.GetComponent<RangeChecker>().playerString = activePlayerString;
					RangeMarkSE.GetComponent<RangeChecker>().rangeIndicator = attackRangeIndicator;
					RangeMarkSE.GetComponent<RangeChecker>().type = "Attack";
					RangeMarkSE.transform.parent = transform;
					generateAttack = false;
				}
				for (int startRange = 0; startRange + nRange < attackRange; startRange++)//South-West
				{
					GameObject RangeMarkSW;
					RangeMarkSW = Instantiate(RangeMarker, new Vector3(transform.position.x + nRange, transform.position.y-0.39f, transform.position.z - startRange - 1), Quaternion.identity) as GameObject;
					RangeMarkSW.GetComponent<RangeChecker>().originBlock = this.gameObject;
					RangeMarkSW.GetComponent<RangeChecker>().playerString = activePlayerString;
					RangeMarkSW.GetComponent<RangeChecker>().rangeIndicator = attackRangeIndicator;
					RangeMarkSW.GetComponent<RangeChecker>().type = "Attack";
					RangeMarkSW.transform.parent = transform;
					generateAttack = false;
				}
				for (int startRange = 0; startRange + nRange < attackRange; startRange++)//North-West
				{
					GameObject RangeMarkNW;
					RangeMarkNW = Instantiate(RangeMarker, new Vector3(transform.position.x - startRange - 1, transform.position.y-0.39f, transform.position.z - nRange), Quaternion.identity) as GameObject;
					RangeMarkNW.GetComponent<RangeChecker>().originBlock = this.gameObject;
					RangeMarkNW.GetComponent<RangeChecker>().playerString = activePlayerString;
					RangeMarkNW.GetComponent<RangeChecker>().rangeIndicator = attackRangeIndicator;
					RangeMarkNW.GetComponent<RangeChecker>().type = "Attack";
					RangeMarkNW.transform.parent = transform;
					generateAttack = false;
				}
				for (int startRange = 0; startRange + nRange < attackRange; startRange++)//North-East
				{
					GameObject RangeMarkNE;
					RangeMarkNE = Instantiate(RangeMarker, new Vector3(transform.position.x - startRange, transform.position.y-0.39f, transform.position.z + nRange + 1), Quaternion.identity) as GameObject;
					RangeMarkNE.GetComponent<RangeChecker>().originBlock = this.gameObject;
					RangeMarkNE.GetComponent<RangeChecker>().playerString = activePlayerString;
					RangeMarkNE.GetComponent<RangeChecker>().rangeIndicator = attackRangeIndicator;
					RangeMarkNE.GetComponent<RangeChecker>().type = "Attack";
					RangeMarkNE.transform.parent = transform;
					generateAttack = false;
				}
			}
		}
	}
}
