﻿using UnityEngine;
using System.Collections;

public class RangeChecker : MonoBehaviour {
	
	public string type = "";
	public bool canMove = true;
	public GameObject originBlock;
	public GameObject activePlayer;
	public GameObject initPlayer;
	public int originRange;
	public int initRange;
	public GameObject Player;
	public string playerString;
	public string direction;
	public Material rangeIndicator;
	// Use this for initialization
	void Start () 
	{
		if(type == "Move")
		{
			initRange = originBlock.GetComponent<RangeFinder>().moveRange;
			GetComponent<Renderer>().material = rangeIndicator;
			RaycastHit inRange;
			int layerMask = 1 << 8;
			if (Physics.Raycast(transform.position, Vector3.down, out inRange, 1.0f, layerMask))
			{
				inRange.collider.GetComponent<FloorUnit>().withinMoveRange = true;
			}
			else
			{
				Destroy(gameObject);
			}
		}
		if(type == "Attack")
		{
			initRange = originBlock.GetComponent<RangeFinder>().attackRange;
			GetComponent<Renderer>().material = rangeIndicator;
			RaycastHit inRange;
			int layerMask = 1 << 8;
			if (Physics.Raycast(transform.position, Vector3.down, out inRange, 1.0f, layerMask))
			{
				inRange.collider.GetComponent<FloorUnit>().withinAttackRange = true;
			}
			else
			{
				Destroy(gameObject);
			}
		}
		initPlayer = originBlock.GetComponent<RangeFinder>().activePlayer;
		activePlayer = originBlock.GetComponent<RangeFinder>().activePlayer;
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(type == "Move")
		{
			originRange = originBlock.GetComponent<RangeFinder>().moveRange;
			if (originRange != initRange)
			{
				originBlock.GetComponent<RangeFinder>().generateMove = true;
				Destroy(gameObject);
			}
			RaycastHit inRange;
			int layerMask = 1 << 8;
			if (Physics.Raycast(transform.position, Vector3.down, out inRange, 1.0f, layerMask))
			{
				inRange.collider.GetComponent<FloorUnit>().withinMoveRange = true;
			}
			else
			{
				Destroy(gameObject);
			}
		}
		if(type == "Attack")
		{
			originRange = originBlock.GetComponent<RangeFinder>().attackRange;
			if (originRange != initRange)
			{
				originBlock.GetComponent<RangeFinder>().generateAttack = true;
				Destroy(gameObject);
			}
			RaycastHit inRange;
			int layerMask = 1 << 8;
			if (Physics.Raycast(transform.position, Vector3.down, out inRange, 1.0f, layerMask))
			{
				inRange.collider.GetComponent<FloorUnit>().withinAttackRange = true;
			}
			else
			{
				Destroy(gameObject);
			}
		}
		if(originBlock.GetComponent<RangeFinder>().FloorController.GetComponent<FloorControl>().TURN > 0)
		{
			Destroy(gameObject);
		}
	}
}
