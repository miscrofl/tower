﻿using UnityEngine;
using System.Collections;

public class FloorUnit : MonoBehaviour {

	#region Pre-load init
	//Master Object
	public GameObject FloorController;
	
	//Reference Objects
	public GameObject prevPlayer;
	public GameObject activePlayer;
	
	//Global Turncount, update sync'd to Floor Controller's TURN int
	public int TURN = 0; 
	
	//Ranges
	public bool withinMoveRange = false;
	public bool withinAttackRange = false;
	
	//Material and Highlight init
	public Material defaultMat;
	public Material highlightedMat;
	public Material highlightedMatGhost;
	public bool highlighted = false;
	public bool highlightedPath = false;
	public bool isSelected = false;
	
	//Location Index init
	public string floorID = "";
	public string activePlayerString;
	public int myTileNumber;
	public int floorZ;
	public int floorX;
	
	//Selection Timeout Check
	public float timer;
	public bool timeOut;
	#endregion
	
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Floor Controller TURN count sync
		TURN = FloorController.GetComponent<FloorControl>().TURN;
		
		//Store prevPlayer for material cancel check and array match
		activePlayer = FloorController.GetComponent<FloorControl>().activePlayer;
		if(TURN == 0)
		{
			prevPlayer = activePlayer;
		}

		//Selection Comparator
		if (FloorController.GetComponent<FloorControl>().selectedFloor_ST != floorID && TURN == 0)
		{
			isSelected = false;
		}
		//Material Control
		if(highlighted == false)
			GetComponent<Renderer>().material = defaultMat;
		if(highlighted == true)
			GetComponent<Renderer>().material = highlightedMat;
		if(highlightedPath == true && highlighted == false && TURN == 0 && activePlayer == prevPlayer)
		{
			GetComponent<Renderer>().material = highlightedMatGhost;
		}
		
		//withinMoveRange check for Range Markers, Range Marker layer 9 = movement, 10 = attack
		RaycastHit inRange;
		int layerMask = 1 << 9;
		if (Physics.Raycast(transform.position, Vector3.up, out inRange, 0.1f, layerMask))
		{
			inRange.collider.GetComponent<FloorUnit>().withinMoveRange = true;
		}
		else
		{
			withinMoveRange = false;
		}
		
		//Clear highlight on mouseclick
		if(highlightedPath == true && Input.GetKeyDown(KeyCode.Mouse0))
		{
			highlightedPath = false;
		}

	}
	
	//Left Click
	void OnMouseDown()
	{
		//activePlayer for selection
		activePlayer = FloorController.GetComponent<FloorControl>().activePlayer;
		
		//Selection
		if (withinMoveRange == true && highlighted == true && FloorController.GetComponent<FloorControl>().TURN == 0)
		{
			//start pathfind function of player to get all blocks between
			activePlayer.GetComponent<PlayerUnit>().Invoke("HighlightPath", 0.3f);
			
			//Select highlighted block and mark it in Floor Controller
			isSelected = true;
			FloorController.GetComponent<FloorControl>().selectedFloor_GO = this.gameObject;
			
			if (activePlayer == FloorController.GetComponent<FloorControl>().Player1)
			{
				FloorController.GetComponent<FloorControl>().p1SelectedFloor_GO = FloorController.GetComponent<FloorControl>().selectedFloor_GO;
				FloorController.GetComponent<FloorControl>().p1SelectedFloor_ST = floorID;
				FloorController.GetComponent<FloorControl>().p1SelectedFloor_Z = floorZ;
				FloorController.GetComponent<FloorControl>().p1SelectedFloor_X = floorX;
			}
			if (activePlayer == FloorController.GetComponent<FloorControl>().Player2)
			{
				FloorController.GetComponent<FloorControl>().p2SelectedFloor_GO = FloorController.GetComponent<FloorControl>().selectedFloor_GO;
				FloorController.GetComponent<FloorControl>().p2SelectedFloor_ST = floorID;
				FloorController.GetComponent<FloorControl>().p2SelectedFloor_Z = floorZ;
				FloorController.GetComponent<FloorControl>().p2SelectedFloor_X = floorX;
			}
			if (activePlayer == FloorController.GetComponent<FloorControl>().Player3)
			{
				FloorController.GetComponent<FloorControl>().p3SelectedFloor_GO = FloorController.GetComponent<FloorControl>().selectedFloor_GO;
				FloorController.GetComponent<FloorControl>().p3SelectedFloor_ST = floorID;
				FloorController.GetComponent<FloorControl>().p3SelectedFloor_Z = floorZ;
				FloorController.GetComponent<FloorControl>().p3SelectedFloor_X = floorX;
			}
			if (activePlayer == FloorController.GetComponent<FloorControl>().Player4)
			{
				FloorController.GetComponent<FloorControl>().p4SelectedFloor_GO = FloorController.GetComponent<FloorControl>().selectedFloor_GO;
				FloorController.GetComponent<FloorControl>().p4SelectedFloor_ST = floorID;
				FloorController.GetComponent<FloorControl>().p4SelectedFloor_Z = floorZ;
				FloorController.GetComponent<FloorControl>().p4SelectedFloor_X = floorX;
			}
			
		}
	}
	void OnMouseEnter()
	{
		if (TURN == 0)
		{
			highlighted = true;
			FloorController.GetComponent<FloorControl>().highlightedFloor = floorID;
			FloorController.GetComponent<FloorControl>().highlightedFloorZ = floorZ;
			FloorController.GetComponent<FloorControl>().highlightedFloorX = floorX;
		}
		else
		{
			highlighted = false;
		}
	}
	void OnMouseExit()
	{
			highlighted = false;
	}
}
