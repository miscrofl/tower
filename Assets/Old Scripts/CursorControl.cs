﻿using UnityEngine;
using System.Collections;

public class CursorControl : MonoBehaviour {

	public GameObject FloorController;
	public GameObject myPlayer;
	public int myPlayer_INT;
	public Material activeMat;
	public Material inactiveMat;
	// Use this for initialization
	void Start () 
	{
		if(myPlayer_INT == 1)
		{
			myPlayer = FloorController.GetComponent<FloorControl>().Player1;
			myPlayer.GetComponent<PlayerUnit>().myCursor = this.gameObject;
		}
		if(myPlayer_INT == 2)
		{
			myPlayer = FloorController.GetComponent<FloorControl>().Player2;
			myPlayer.GetComponent<PlayerUnit>().myCursor = this.gameObject;
		}
		if(myPlayer_INT == 3)
		{
			myPlayer = FloorController.GetComponent<FloorControl>().Player3;
			myPlayer.GetComponent<PlayerUnit>().myCursor = this.gameObject;
		}
		if(myPlayer_INT == 4)
		{
			myPlayer = FloorController.GetComponent<FloorControl>().Player4;
			myPlayer.GetComponent<PlayerUnit>().myCursor = this.gameObject;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Rotate (Vector3.up * Time.deltaTime * 120);
		float speed = 8;
		float dist = speed * Vector3.Distance(FloorController.GetComponent<FloorControl>().activePlayer.transform.position, transform.position) + 12;
		if(myPlayer == FloorController.GetComponent<FloorControl>().activePlayer  && FloorController.GetComponent<FloorControl>().p1SelectedFloor_GO != null)
		{
			GetComponent<Renderer>().material = activeMat;
			if (myPlayer == FloorController.GetComponent<FloorControl>().Player1)
			{
				Vector3 selectionTrack = FloorController.GetComponent<FloorControl>().p1SelectedFloor_GO.transform.position;
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
			if (myPlayer == FloorController.GetComponent<FloorControl>().Player2)
			{
				Vector3 selectionTrack = FloorController.GetComponent<FloorControl>().p2SelectedFloor_GO.transform.position;
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
			if (myPlayer == FloorController.GetComponent<FloorControl>().Player3)
			{
				Vector3 selectionTrack = FloorController.GetComponent<FloorControl>().p3SelectedFloor_GO.transform.position;
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
			if (myPlayer == FloorController.GetComponent<FloorControl>().Player4)
			{
				Vector3 selectionTrack = FloorController.GetComponent<FloorControl>().p4SelectedFloor_GO.transform.position;
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
		}
		else
		{
			GetComponent<Renderer>().material = inactiveMat;
			if (myPlayer == FloorController.GetComponent<FloorControl>().Player1)
			{
				Vector3 selectionTrack = FloorController.GetComponent<FloorControl>().p1SelectedFloor_GO.transform.position;
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
			if (myPlayer == FloorController.GetComponent<FloorControl>().Player2)
			{
				Vector3 selectionTrack = FloorController.GetComponent<FloorControl>().p2SelectedFloor_GO.transform.position;
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
			if (myPlayer == FloorController.GetComponent<FloorControl>().Player3)
			{
				Vector3 selectionTrack = FloorController.GetComponent<FloorControl>().p3SelectedFloor_GO.transform.position;
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
			if (myPlayer == FloorController.GetComponent<FloorControl>().Player4)
			{
				Vector3 selectionTrack = FloorController.GetComponent<FloorControl>().p4SelectedFloor_GO.transform.position;
				selectionTrack.y = 2;
				transform.position = Vector3.MoveTowards(transform.position, selectionTrack, dist * Time.deltaTime);
			}
		}
	}
}
