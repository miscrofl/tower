﻿using UnityEngine;
using System.Collections;

public class PlayerUnit : MonoBehaviour {
	
	public bool DONE = false;
	public bool canMove = false;
	public bool ACTIVE = false;
	public int TURN = 0;
	public GameObject FloorController;
	public GameObject myCursor;
	public GameObject selection;
	public GameObject nextPlayer;
	public string playerNumber;
	public string tileNumber;
	public int tileZ;
	public int tileX;
	public int totalMoves;
	public int realXRange;
	public int realYRange;
	public int moveRange;
	public int attackRange;
	public bool withinRange;
	public string[] adjacentFloors = new string[4];//NESW ordered adjacent floors in said direction
	public GameObject[] targetFloor;
	public GameObject selectedFloor_GO;
	public int selectedFloorZ;
	public int selectedFloorX;
	public int nTileX;
	public int nTileY;
	public int eTileX;
	public int eTileY;
	public int sTileX;
	public int sTileY;
	public int wTileX;
	public int wTileY;
	public string nTileNumber;
	public string eTileNumber;
	public string sTileNumber;
	public string wTileNumber;
	public Vector3 northMove;
	public Vector3 eastMove;
	public Vector3 southMove;
	public Vector3 westMove;
	
	
	// Use this for initialization
	void Start () 
	{
		if (FloorController.GetComponent<FloorControl>().Player1 == this.gameObject)
			playerNumber = "Player 1";
		if (FloorController.GetComponent<FloorControl>().Player2 == this.gameObject)
			playerNumber = "Player 2";
		if (FloorController.GetComponent<FloorControl>().Player3 == this.gameObject)
			playerNumber = "Player 3";
		if (FloorController.GetComponent<FloorControl>().Player4 == this.gameObject)
			playerNumber = "Player 4";
		
		Invoke("TileGet", 0);
		targetFloor = new GameObject[100];

	}
	void FixedUpdate ()
	{
	}
	// Update is called once per frame
	void Update () 
	{
		int TURN = FloorController.GetComponent<FloorControl>().TURN;
		if (playerNumber == "Player 1")
		{
			selectedFloorZ = FloorController.GetComponent<FloorControl>().p1SelectedFloor_Z;
			selectedFloorX = FloorController.GetComponent<FloorControl>().p1SelectedFloor_X;
			selectedFloor_GO = FloorController.GetComponent<FloorControl>().p1SelectedFloor_GO;
		}
		if (playerNumber == "Player 2")
		{
			selectedFloorZ = FloorController.GetComponent<FloorControl>().p2SelectedFloor_Z;
			selectedFloorX = FloorController.GetComponent<FloorControl>().p2SelectedFloor_X;
			selectedFloor_GO = FloorController.GetComponent<FloorControl>().p2SelectedFloor_GO;
		}
		if (playerNumber == "Player 3")
		{
			selectedFloorZ = FloorController.GetComponent<FloorControl>().p3SelectedFloor_Z;
			selectedFloorX = FloorController.GetComponent<FloorControl>().p3SelectedFloor_X;
			selectedFloor_GO = FloorController.GetComponent<FloorControl>().p3SelectedFloor_GO;
		}
		if (playerNumber == "Player 4")
		{
			selectedFloorZ = FloorController.GetComponent<FloorControl>().p4SelectedFloor_Z;
			selectedFloorX = FloorController.GetComponent<FloorControl>().p4SelectedFloor_X;
			selectedFloor_GO = FloorController.GetComponent<FloorControl>().p4SelectedFloor_GO;
		}
		Vector3 thisPlayer = transform.position;
		Vector3 floorHeight = transform.position;
		floorHeight.y -= 1;
		float cursorDist = Vector3.Distance(myCursor.transform.position, transform.position);
		if (cursorDist > 0f)
		{
			Vector3 targetDir = selectedFloor_GO.transform.position - thisPlayer;
			float step = 0.5f * Time.deltaTime;
			Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
			Debug.DrawRay(thisPlayer, newDir, Color.red);
			float rotTo = Quaternion.LookRotation(newDir).y;
			Vector3 finalDir = newDir;
			finalDir.y = transform.position.y;
			transform.rotation = Quaternion.LookRotation(finalDir);
		}
		if (ACTIVE == true)
		{
		
		}
		//
		if (TURN == 0)
		{
			if (Input.GetKeyUp(KeyCode.Period) && ACTIVE == true)
			{
				moveRange ++;
			}
			if (Input.GetKeyUp(KeyCode.Comma) && ACTIVE == true)
			{
				moveRange --;
			}	
			float targetDistCursor = Vector3.Distance(transform.position, myCursor.transform.position);
			if(targetDistCursor < 0.03f)
			{
				DONE = true;
			}
			else
			{
				DONE = false;
			}
		}
		//
		if (TURN > 0 && DONE == false)
		{
			float targetDistCursor = Vector3.Distance(transform.position, myCursor.transform.position);
			if(targetDistCursor < -1f)
			{
				FloorController.GetComponent<FloorControl>().TURN = TURN + 1;
				transform.position = myCursor.transform.position;
				Invoke("TileGet", 0);
				targetFloor[1] = myCursor;
				DONE = true;
			}
			print("Turn " + TURN);
			if(canMove == true)
			{
				if(tileX > targetFloor[TURN].transform.position.x && tileZ ==  targetFloor[TURN].transform.position.z)//northMove
				{
					transform.position = Vector3.MoveTowards(transform.position, northMove, 5*Time.deltaTime);
					float dist = Vector3.Distance(transform.position, northMove);
					if(dist < 0.01f)
					{
						FloorController.GetComponent<FloorControl>().TURN = TURN + 1;
						Invoke("TileGet", 0.2f);
						canMove = false;
						transform.position = northMove;
					}
				}
				if(tileX < targetFloor[TURN].transform.position.x && tileZ ==  targetFloor[TURN].transform.position.z)//southMove
				{
					transform.position = Vector3.MoveTowards(transform.position, southMove, 5*Time.deltaTime);
					float dist = Vector3.Distance(transform.position, southMove);
					if(dist < 0.01f)
					{
						FloorController.GetComponent<FloorControl>().TURN = TURN + 1;
						Invoke("TileGet", 0.2f);
						canMove = false;
						transform.position = southMove;
					}
				}
				if(tileZ > targetFloor[TURN].transform.position.z)//westMove
				{
					transform.position = Vector3.MoveTowards(transform.position, westMove, 5*Time.deltaTime);
					float dist = Vector3.Distance(transform.position, westMove);
					if(dist < 0.01f)
					{
						FloorController.GetComponent<FloorControl>().TURN = TURN + 1;
						Invoke("TileGet", 0.2f);
						canMove = false;
						transform.position = westMove;
					}
					}
				if(tileZ < targetFloor[TURN].transform.position.z)//eastMove
				{
					transform.position = Vector3.MoveTowards(transform.position, eastMove, 5*Time.deltaTime);
					float dist = Vector3.Distance(transform.position, eastMove);
					if(dist < 0.01f)
					{
						FloorController.GetComponent<FloorControl>().TURN = TURN + 1;
						Invoke("TileGet", 0.2f);
						canMove = false;
						transform.position = eastMove;
					}
				}
			}
		}

	}
	//
	void OnMouseDown()
	{
		FloorController.GetComponent<FloorControl>().activePlayer = this.gameObject;
	}
	//
	void HighlightPath ()
	{
		RaycastHit[] hits;
		Vector3 floorPos = transform.position;
		floorPos.y = 1;
		floorPos.z = transform.position.z + 0.2f;
		int layerMask = 1 << 8;
		Vector3 forward = transform.TransformDirection(Vector3.down);
		hits = Physics.RaycastAll(floorPos, forward, Vector3.Distance(transform.position, myCursor.transform.position), layerMask);
		Debug.DrawRay(transform.position, forward, Color.red);
		for (int i = 0; i < hits.Length; i++)
		{
			RaycastHit hit = hits[i];
			hit.collider.gameObject.GetComponent<FloorUnit>().highlightedPath = true;
			hit.collider.gameObject.GetComponent<FloorUnit>().myTileNumber = i;
			targetFloor[i+1] = hit.collider.gameObject;
 		}
		CancelInvoke("Highlight");
	}
	//
	void TileGet ()
	{
		RaycastHit currentTile;
		if (Physics.Raycast(transform.position, Vector3.down, out currentTile))
		{
			tileNumber = currentTile.collider.GetComponent<FloorUnit>().floorID;
			tileZ = currentTile.collider.GetComponent<FloorUnit>().floorZ;
			tileX = currentTile.collider.GetComponent<FloorUnit>().floorX;
		}
		northMove = transform.position;
		northMove.x -= 1;
		eastMove = transform.position;
		eastMove.z += 1;
		southMove = transform.position;
		southMove.x += 1;
		westMove = transform.position;
		westMove.z -= 1;
		nTileX = tileZ;
		nTileY = tileX -1;
		eTileX = tileZ +1;
		eTileY = tileX;
		sTileX = tileZ;
		sTileY = tileX +1;
		wTileX = tileZ -1;
		wTileY = tileX;
		nTileNumber = (nTileX + "," + nTileY);
		eTileNumber = (eTileX + "," + eTileY);
		sTileNumber = (sTileX + "," + sTileY);
		wTileNumber = (wTileX + "," + wTileY);
		adjacentFloors[0] = nTileNumber;
		adjacentFloors[1] = eTileNumber;
		adjacentFloors[2] = sTileNumber;
		adjacentFloors[3] = wTileNumber;
		canMove = true;
		if(playerNumber == "Player 1")
		{
			nextPlayer = FloorController.GetComponent<FloorControl>().Player1;
		}
		if(playerNumber == "Player 2")
		{
			nextPlayer = FloorController.GetComponent<FloorControl>().Player2;
		}
		if(playerNumber == "Player 3")
		{
			nextPlayer = FloorController.GetComponent<FloorControl>().Player3;
		}
		if(playerNumber == "Player 4")
		{
			nextPlayer = FloorController.GetComponent<FloorControl>().Player4;
		}
		if(DONE == true && nextPlayer.GetComponent<PlayerUnit>().DONE == false)
		{
			FloorController.GetComponent<FloorControl>().activePlayer = nextPlayer;
			FloorController.GetComponent<FloorControl>().TURN = 1;
		}
		CancelInvoke("TileGet");
	}
	
}
