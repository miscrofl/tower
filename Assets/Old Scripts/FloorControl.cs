﻿using UnityEngine;
using System.Collections;

public class FloorControl : MonoBehaviour {

	public int TURN = 0;
	public bool turnLock = false;
	public string selectedFloor_ST;
	public GameObject selectedFloor_GO;
	public int playerCount = 4;
	public GameObject p1SelectedFloor_GO = null;
	public GameObject p2SelectedFloor_GO = null;
	public GameObject p3SelectedFloor_GO = null;
	public GameObject p4SelectedFloor_GO = null;
	public GameObject Player1;
	public GameObject Player2;
	public GameObject Player3;
	public GameObject Player4;
	public GameObject activePlayer;
	public string activePlayer_ST;
	public int activePlayer_INT;
	public string highlightedFloor = "";
	public int highlightedFloorZ = 0;
	public int highlightedFloorX = 0;
	public string p1SelectedFloor_ST;
	public string p2SelectedFloor_ST;
	public string p3SelectedFloor_ST;
	public string p4SelectedFloor_ST;
	public int p1SelectedFloor_Z;
	public int p1SelectedFloor_X;
	public int p2SelectedFloor_Z;
	public int p2SelectedFloor_X;
	public int p3SelectedFloor_Z;
	public int p3SelectedFloor_X;
	public int p4SelectedFloor_Z;
	public int p4SelectedFloor_X;
	public int rWidth;
	public int rHeight;
	public GameObject Floors;
	public GameObject Cursors;
	public GameObject RangeFinder;
	public Material activeMat;
	public Material inactiveMat;
	// Use this for initialization
	void Start () 
	{
		p1SelectedFloor_GO = this.gameObject;
		p2SelectedFloor_GO = this.gameObject;
		p3SelectedFloor_GO = this.gameObject;
		p4SelectedFloor_GO = this.gameObject;
		//
		for (int wi = 1; wi < rHeight; wi++)
		{
			for (int hi = 1; hi < rWidth; hi++)
			{
				GameObject Floor;
				Floor = Instantiate(Floors, new Vector3(transform.position.x + hi, transform.position.y-0.48f, transform.position.z + wi), Quaternion.identity) as GameObject;
				Floor.GetComponent<FloorUnit>().FloorController = this.gameObject;
				Floor.GetComponent<FloorUnit>().floorZ = wi;
				Floor.GetComponent<FloorUnit>().floorX = hi;
				Floor.GetComponent<FloorUnit>().floorID = wi + "," + hi;
				Floor.transform.parent = transform;
			}		
		}
		//
		GameObject p1Cursor;
		p1Cursor = Instantiate(Cursors, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity) as GameObject;
		p1Cursor.GetComponent<CursorControl>().FloorController = this.gameObject;
		p1Cursor.GetComponent<CursorControl>().myPlayer = Player1;
		p1Cursor.GetComponent<CursorControl>().activeMat = activeMat;
		p1Cursor.GetComponent<CursorControl>().inactiveMat = inactiveMat;
		p1Cursor.GetComponent<CursorControl>().myPlayer_INT = 1;
		//
		GameObject p2Cursor;
		p2Cursor = Instantiate(Cursors, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity) as GameObject;
		p2Cursor.GetComponent<CursorControl>().FloorController = this.gameObject;
		p2Cursor.GetComponent<CursorControl>().myPlayer = Player2;
		p2Cursor.GetComponent<CursorControl>().activeMat = activeMat;
		p2Cursor.GetComponent<CursorControl>().inactiveMat = inactiveMat;
		p2Cursor.GetComponent<CursorControl>().myPlayer_INT = 2;
		//
		GameObject p3Cursor;
		p3Cursor = Instantiate(Cursors, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity) as GameObject;
		p3Cursor.GetComponent<CursorControl>().FloorController = this.gameObject;
		p3Cursor.GetComponent<CursorControl>().myPlayer = Player3;
		p3Cursor.GetComponent<CursorControl>().activeMat = activeMat;
		p3Cursor.GetComponent<CursorControl>().inactiveMat = inactiveMat;
		p3Cursor.GetComponent<CursorControl>().myPlayer_INT = 3;
		//
		GameObject p4Cursor;
		p4Cursor = Instantiate(Cursors, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity) as GameObject;
		p4Cursor.GetComponent<CursorControl>().FloorController = this.gameObject;
		p4Cursor.GetComponent<CursorControl>().myPlayer = Player4;
		p4Cursor.GetComponent<CursorControl>().activeMat = activeMat;
		p4Cursor.GetComponent<CursorControl>().inactiveMat = inactiveMat;
		p4Cursor.GetComponent<CursorControl>().myPlayer_INT = 4;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if(activePlayer == Player1)
		{
			activePlayer_ST = "Player 1";
			activePlayer_INT = 1;
			RangeFinder.GetComponent<RangeFinder>().activePlayer = Player1;
			Player1.GetComponent<PlayerUnit>().ACTIVE = true;
			Player2.GetComponent<PlayerUnit>().ACTIVE = false;
			Player3.GetComponent<PlayerUnit>().ACTIVE = false;
			Player4.GetComponent<PlayerUnit>().ACTIVE = false;
	
		}
		if(activePlayer == Player2)
		{
			activePlayer_ST = "Player 2";
			activePlayer_INT = 2;
			RangeFinder.GetComponent<RangeFinder>().activePlayer = Player2;
			Player1.GetComponent<PlayerUnit>().ACTIVE = false;
			Player2.GetComponent<PlayerUnit>().ACTIVE = true;
			Player3.GetComponent<PlayerUnit>().ACTIVE = false;
			Player4.GetComponent<PlayerUnit>().ACTIVE = false;
		}
		if(activePlayer == Player3)
		{
			
			activePlayer_ST = "Player 3";
			activePlayer_INT = 3;
			RangeFinder.GetComponent<RangeFinder>().activePlayer = Player3;
			Player1.GetComponent<PlayerUnit>().ACTIVE = false;
			Player2.GetComponent<PlayerUnit>().ACTIVE = false;
			Player3.GetComponent<PlayerUnit>().ACTIVE = true;
			Player4.GetComponent<PlayerUnit>().ACTIVE = false;
		}
		if(activePlayer == Player4)
		{
			activePlayer_ST = "Player 4";
			activePlayer_INT = 4;
			RangeFinder.GetComponent<RangeFinder>().activePlayer = Player4;
			Player1.GetComponent<PlayerUnit>().ACTIVE = false;
			Player2.GetComponent<PlayerUnit>().ACTIVE = false;
			Player3.GetComponent<PlayerUnit>().ACTIVE = false;
			Player4.GetComponent<PlayerUnit>().ACTIVE = true;
		}
		if(Input.GetKeyUp(KeyCode.RightControl) && TURN == 0)
		{
			TURN = 1;
			print("Turn beginning");
			activePlayer.GetComponent<PlayerUnit>().canMove = true;
		}
		if(Input.GetKeyUp(KeyCode.LeftControl) && TURN != 0)
		{
			TURN = 0;
			print("Turn reset");
			activePlayer.GetComponent<PlayerUnit>().canMove = false;
			Player1.GetComponent<PlayerUnit>().DONE = false;
			Player2.GetComponent<PlayerUnit>().DONE = false;
			Player3.GetComponent<PlayerUnit>().DONE = false;
			Player4.GetComponent<PlayerUnit>().DONE = false;
			Player1.GetComponent<PlayerUnit>().Invoke("TileGet", 0);
			Player2.GetComponent<PlayerUnit>().Invoke("TileGet", 0);
			Player3.GetComponent<PlayerUnit>().Invoke("TileGet", 0);
			Player4.GetComponent<PlayerUnit>().Invoke("TileGet", 0);
			RangeFinder.GetComponent<RangeFinder>().generateMove = true;
			RangeFinder.GetComponent<RangeFinder>().generateAttack = true;
			
		}

	}
}
